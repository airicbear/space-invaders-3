/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;
import engine.Vector2D;

/**
 * @author Eric Nguyen
 */
public class Ship extends GameObject implements Character {
    
  private int score, lives;
  private GameObject projectile;
  private Vector2D initPosition;

  public Ship() {
    super("file:Sprites/SpaceShip.png");
    
    // Initialize variables
    lives = 3;
    movementSpeed = 5;
    projectile = new Projectile("file:Sprites/Projectile_0.png");

    // Hide projectile
    ((Projectile) projectile).hide();
  }

  public Ship(float x, float y) {
    this();
    position = new Vector2D(x, y);
    initPosition = new Vector2D(x, y);
  }
  
  @Override
  public void shoot() {
    // Instantiate a new projectile clone
    GameObject projectileClone = new Projectile("file:Sprites/Projectile_0.png");
    
    // Adjust the projectile clone's position to line up with the end of the ship's cannon
    projectileClone.position = position.add(Vector2D.RIGHT.multiply(scale.x / 2 - projectileClone.scale.x / 2));
    
    // Set the velocity of the projectile clone so that it will constantly move upwards
    projectileClone.velocity = Vector2D.UP.multiply(projectileClone.movementSpeed);
    
    // Assign the projectile clone to the ship's projectile variable
    projectile = projectileClone;
  }

  @Override
  public void die() {
    lives = 0;
    velocity = Vector2D.ZERO;
  }

  @Override
  public boolean isAlive() {
    return lives > 0;
  }

  @Override
  public GameObject getProjectile() {
    return projectile;
  }
  
  /**
   * Decrement the number of this ship's lives and reset its position
   */
  public void hit() {
    lives--;
  }
    
  public int getScore() {
    return score;
  }

  public void addScore(int n) {
    score += n;
  }

  public int getLives() {
    return lives;
  }

}
