/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders.assets;

import engine.GameObject;

/**
 *
 * @author Eric Nguyen
 */
public interface Character {    
    
  /**
   * Move this character's position by its velocity multiplied by its movement speed. (position = velocity * movementSpeed)
   * <br>
   * If this character's velocity is equal to Vector2D.ZERO, it will not move.
   */
  void move();

  /**
   * Create a new projectile and set its velocity to the direction which it will shoot
   */
  void shoot();

  /**
   * Temporarily replace this character's image and then scale it down to Vector2D.ZERO
   */
  void die();

  /**
   * @return Character's projectile
   */
  GameObject getProjectile();

  /**
   * @return true if this character is alive
   */
  boolean isAlive();
    
}
